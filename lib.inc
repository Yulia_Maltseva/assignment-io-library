section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi + rax], 0  	; Проверка, является ли текущий символ строки 0
    je .end                  	; Выход из цикла
    inc rax                  	; Иначе i++
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall	
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi			        ; Ложим символ в вершину стека
    mov rdi, rsp		        ; Указываем на вершину стека, т.е. на символ, который нужно напечатать
    call print_string		        ; Вызываем print_string с адресом rdi = rsp
    pop rdi			        ; Берем со стека
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r9, 10
    mov rcx, rsp
    dec rsp
    mov byte [rsp], 0
.loop:
    xor rdx, rdx
    div r9
    add rdx, '0'
    dec rsp
    mov byte [rsp], dl
    test rax, rax
    jnz .loop

    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    mov rsp, rcx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .print
    push rdi
    mov rdi, "-"
    call print_char
    pop rdi
    neg rdi
.print:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor r9, r9
    xor r10, r10
.loop:
    mov r9b, byte [rdi + rcx]
    mov r10b, byte [rsi + rcx]
    cmp r9b, r10b
    jne .neq
    cmp r9b, 0
    je .eq
    inc rcx
    jmp .loop
.eq:                 		    ; Если equals
    mov rax, 1
    ret
.neq:                		    ; Если not equals
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax		
    push rax
    mov rsi, rsp
    mov rdi, 0
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
.loop:
    push rdx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rdx
    cmp rax, 0
    je .success
    cmp rdx, 0
    jne .next
    cmp rax, 0xA
    je .loop
    cmp rax, 0x9
    je .loop
    cmp rax, 0x20
    je .loop
.next:
    cmp rdx, rsi
    je .fail
    cmp rax, 0xA
    je .success
    cmp rax, 0x9
    je .success
    cmp rax, 0x20
    je .success
    mov r8, rax
    mov byte[rdx+rdi], r8b
    inc rdx
    jmp .loop
.success:
    mov byte[rdx+rdi], 0
    mov rax, rdi
    ret
.fail:
    mov rax, 0
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r9, 10
    xor rsi, rsi
    xor rdx, rdx

.loop:
    movzx r8, byte[rdi + rcx]
    cmp r8b, '0'
    jb .end
    cmp r8b, '9'
    ja .end
    xor rdx, rdx
    mul r9
    sub r8b, '0'
    add rax, r8
    inc rcx
    jmp .loop
.end:
    mov rdx, rcx
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    je .neg
    jmp parse_uint
.neg: 
    inc rdi
    call parse_uint	
    inc rdx
    neg rax
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx            
    xor r9, r9
    call string_length
    push rax
    push rsi
.loop:
    cmp rcx, rdx
    je .zero
    mov r9, [rdi+rcx]
    mov [rsi+rcx], r9
    cmp rax,0
    je .end
    dec rax
    inc rcx
    jmp .loop
.zero:
    pop rsi
    pop rax
    mov rax,0
    ret
.end:
    pop rsi
    pop rax
    mov byte [rsi+rax],0
    ret
